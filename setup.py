#!/usr/bin/python3

from setuptools import setup
setup(name='python-teleboy',
      version='1.0',
      py_modules=['teleboy'],
      install_requires=[
            'requests',
            'simplejson',
      ]
      )
