#!/usr/bin/python3
import os
import base64
import http.cookiejar
import urllib
import urllib.request
import urllib.error
from urllib.parse import urlencode
import requests
import simplejson
import time

TB_URL = "https://www.teleboy.ch"
API_URL = "https://tv.api.teleboy.ch"
API_KEY = base64.b64decode(
        "ZGI5NTAxZDY0NjMyYTk0NGYxYjk4NGQ3YWNhZjBiOTgzYzViZmNhYTcyM2E4ZGZjZjI0ZGQ5NTEzNTRkMTg3OA==")  # noqa: E501


def ensure_login(func):
    def func_wrapper(self, *args, **kwargs):
        self._ensure_login()
        return func(self, *args, **kwargs)
    return func_wrapper


class Teleboy(object):
    def __init__(self):
        self.username = ""
        self.password = ""
        self.user_id = None
        self._resources_path = "/tmp/teleboy/"
        self._cookie_file = self._resources_path + "cookie.dat"

        self.cookies = http.cookiejar.LWPCookieJar(self._cookie_file)
        self.cookies_dict = {}
        self._parse_cookies()

        opener = urllib.request.build_opener(
                urllib.request.HTTPCookieProcessor(self.cookies))
        urllib.request.install_opener(opener)

        if not os.path.exists(self._resources_path):
            os.mkdir(self._resources_path)

        self.titles = None
        self.recordings = None

    def _parse_cookies(self):
        self.cookies_dict = requests.utils.dict_from_cookiejar(self.cookies)

    def _save_cookies_to_file(self):
        self.cookies.save(ignore_discard=True)
        self._parse_cookies()

    def _clear_cookies(self):
        self.cookies.clear()
        self.cookies_dict = {}

    def _fetchHttp(self, url, args={}, hdrs={}, method="GET"):
        hdrs["User-Agent"] = "Mozilla/5.0 (X11; Linux i686; rv:5.0) Gecko/20100101 Firefox/5.0"  # noqa: E501
        if method == "POST":
            req = urllib.request.Request(url, urlencode(args).encode("utf-8"), hdrs)
        elif method == "DELETE":
            req = urllib.request.Request(url, None, hdrs)
            req.get_method = lambda: 'DELETE'
        else:
            url = url + "?" + urlencode(args)
            req = urllib.request.Request(url, None, hdrs)
        try:
            response = urllib.request.urlopen(req)
        except Exception as e:
            if method == "DELETE":
                return '{"data": ""}'
            else:
                raise e
        text = response.read()
        responsetext = text.decode('utf8')
        response.close()

        return responsetext

    def _fetchHttpWithCookies(self, url, args={}, hdrs={}, method="GET"):
        url = API_URL + "/users/%s/" % self.user_id + url
        hdrs["x-teleboy-apikey"] = API_KEY
        hdrs["x-teleboy-session"] = self.cookies_dict["cinergy_s"]
        ans = self._fetchHttp(url, args, hdrs, method)
        return simplejson.loads(ans)["data"]

    def _ensure_login(self):
        if "cinergy_auth" not in self.cookies_dict or \
                "cinergy_s" not in self.cookies_dict:
            self.login()

    def login(self):
        self._clear_cookies()

        url = TB_URL + "/login_check"
        args = {"login": self.username,
                "password": self.password,
                "keep_login": "1"}
        hdrs = {"Referer": TB_URL}

        reply = self._fetchHttp(url, args, hdrs, method="POST")

        if "Falsche Eingaben" in reply \
                or "Anmeldung war nicht erfolgreich" in reply \
                or "Bitte melde dich neu an" in reply:
            raise Exception("Login failed")
            return False
        else:
            self.user_id = self._extract_user_id(reply)
            self._save_cookies_to_file()
            return True

    def _extract_user_id(self, html):
        # extract user id
        user_id = ""
        lines = html.split('\n')
        for line in lines:
            if "setId" in line:
                dummy, uid = line.split("(")
                user_id = uid[:-1]
                break
        return user_id

    @ensure_login
    def update(self):
        url = "records/ready"
        args = {"expand": "station",
                "limit": 500,
                "skip": 0}
        data = self._fetchHttpWithCookies(url, args)
        self.titles = {}
        self.recordings = {}
        for item in data["items"]:
            title = item["title"]
            if title not in self.titles:
                self.titles[title] = {}
            self.titles[title][item["id"]] = item
            self.recordings[item["id"]] = item

    def get_title_set(self):
        return self.titles.keys()

    def get_broadcasts(self, title=None):
        if title:
            if title in self.titles:
                return self.titles[title]
            else:
                return {}
        else:
            return self.recordings

    @ensure_login
    def get_info(self, recid):
        broadcast_id = self.recordings[recid]["broadcast_id"]
        url = "broadcasts/{}".format(broadcast_id)
        args = {"expand": "previewImage"}
        return self._fetchHttpWithCookies(url, args)

    def get_stream(self, recid):
        url = "stream/record/%s" % recid
        return self._fetchHttpWithCookies(url)

    def get_live(self):
        url = "broadcasts/now"
        args = {"expand": "flags,station,previewImage", "stream": True}
        return self._fetchHttpWithCookies(url, args)

    def get_live_stream(self, sid):
        url = "stream/live/%s" % sid
        args = {"alternative": "false"}
        return self._fetchHttpWithCookies(url, args)

    def delete_all(self, title):
        broadcasts = self.get_broadcasts(title)
        for recid in broadcasts:
            self.delete(recid)

    def delete(self, recid):
        url = "records/%s" % recid
        self._fetchHttpWithCookies(url, method="DELETE")

    def download(self, recid, output_path):
        def request_download():
            broadcast_id = self.recordings[recid]["broadcast_id"]
            url = "recordings/{}/download".format(broadcast_id)
            data = self._fetchHttpWithCookies(url)

            options = data["options"]
            for option in options:
                if option["name"] == "medium":
                    profile = option["profile"]
                    break
            url = url + "/{}?alternative=0".format(profile)
            return url

        def wait_download_ready(url):
            hdrs = {}
            hdrs["User-Agent"] = "Mozilla/5.0 (X11; Linux i686; rv:5.0) Gecko/20100101 Firefox/5.0"  # noqa: E501
            hdrs["x-teleboy-apikey"] = API_KEY
            hdrs["x-teleboy-session"] = self.cookies_dict["cinergy_s"]

            code = 400
            while code != 200:
                try:
                    req = urllib.request.Request(url, None, hdrs)
                    response = urllib.request.urlopen(req)
                    text = response.read()
                    code = response.getcode()
                except urllib.error.HTTPError as e:
                    if e.code != 400:
                        raise e
                    time.sleep(5)
            response.close()
            ans = text.decode('utf8')
            data = simplejson.loads(ans)
            return data["data"]["url"]

        url = request_download()
        download_url = wait_download_ready(url)
        urllib.request.urlretrieve(download_url, output_path)
